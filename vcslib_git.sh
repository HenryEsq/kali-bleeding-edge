vcs_parse_options() {
    vcs_git_branch="$1"
}

vcs_get_current_revision() {
    local gitref="${vcs_git_branch:-HEAD}"
    commit=$(git ls-remote "$1" "$gitref" | awk '{ if ($2 == "'"$gitref"'") print $1 }')
    if [ -z "$commit" ]; then
	echo "ERROR: could not find branch $gitref on $1" >&2
	exit 1
    fi
    echo $commit
}

vcs_update() {
    git pull
}

vcs_download() {
    if [ -n "$vcs_git_branch" ]; then
	git clone -b ${vcs_git_branch##refs/heads/} $1 $2
    else
	git clone $1 $2
    fi
}

vcs_export() {
    mkdir -p $2
    git archive --format=tar HEAD | (cd $2 && tar xf -)
}

vcs_version_part() {
    date=$(date +%s)
    short_commit=$(echo "$1" | cut -b -6)
    echo "$date.$short_commit"
}
